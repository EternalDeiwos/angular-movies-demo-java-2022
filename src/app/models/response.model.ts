export interface MovieResponse<T> {
    data: T;
    success: boolean;
    error?: string;
}