export interface Movie {
    id: string;
    title: string;
    description: string;
    cover: string;
    rating: string;
    genre: string[],
    director: string;
    cast: string[];
    active: number;
}

/*export interface MovieResponse {
    success: boolean;
    data: Movie[];
}

export interface MovieDetailResponse {
    success: boolean;
    data: Movie;
}
*/

