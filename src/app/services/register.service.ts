import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MovieResponse } from '../models/response.model';
import { User } from '../models/user.model';

const URL = environment.moviesAPI;

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor(private http: HttpClient) {}

  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': 'alskdjalksjdlkasjkdl',
    });
  }

  // Create a new user! WOW! 🤩
  register(username: string, password: string): Observable<boolean> {
    const user = {
      user: { username, password },
    };
    const headers = this.createHeaders();
    ////// RETURNING AN OBSV.
    return this.http
      .post<MovieResponse<User>>(URL + '/users/register', user, {
        headers,
      })
      .pipe(
        map((response: MovieResponse<User>) => {
          if (response.success === false) {
            // throw new Error(response.error);
            throwError(() => response.error);
          }
          return response.success;
        }),

      );
  }
}
