import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';

@Component({
  selector: 'app-movie-list-item',
  templateUrl: './movie-list-item.component.html',
  styleUrls: ['./movie-list-item.component.css']
})
export class MovieListItemComponent implements OnInit {

  // The ! tells TypeScript this movie will never be undefined. 
  @Input() movie!: Movie;

  constructor() { }

  ngOnInit(): void {
  }

  onWatchlistClick() {
    console.log("You can add it to a watchlist service");
  }

}
