# Angular Movies

This repo was part of a Angular Demo for Java's Front-end group 2022.

## Usage

Install dependencies

```bash
npm install
````

Run locally on port 4200. (-o automatically opens in default browser)

```bash
ng serve -o
```

## API Documentation

[Movie API Documentation](https://documenter.getpostman.com/view/1167753/Tz5s5cpf)

## Author

Dewald Els

## Features

Outlined some features of the application.

### Pages

#### Register

Register a new user using a POST request to the movies api

#### Movies

Download a list of 100 movies and display in an "Image Grid" the screen.

#### Movie Details (WIP)

Download a specific movie based on its ID and display more information. (WIP)

## Additional features

### Watchlist

Add some code to add the movie to the users' favourites. You can use the [Favourites endpoint](https://documenter.getpostman.com/view/1167753/Tz5s5cpf#1a43517c-bf73-4c8f-a855-7d848b599f0d) from the API Docs.
